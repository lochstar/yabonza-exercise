import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { Celeb } from './celeb';

const celebrityList = require('../assets/richList.json').celebrityList;

@Injectable()
export class CelebService {

  constructor() { }

  getCelebs(birthplaceValue: string, searchValue: string, orderByValue): Observable<Celeb[]> {
    // filter by search value & birth place
    const s = searchValue.toLowerCase();
    const searchResult = celebrityList.filter((celeb) => {
      return Object.keys(celeb).some((key) => {
        return celeb[key].toString().toLowerCase().indexOf(s) > -1 && (birthplaceValue !== 'Show All' ? celeb.country === birthplaceValue : true);
      });
    });

    // string sort (a-z)
    if (orderByValue === 'name') {
      searchResult.sort((a, b) => a[orderByValue].localeCompare(b[orderByValue]));

    // number sort (lowest first)
    } else {
      searchResult.sort((b, a) => b[orderByValue] - a[orderByValue]);
    }

    return of(searchResult);
  }
}
