import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { Celeb } from '../celeb';

const currencyFormatter = require('currency-formatter');

@Component({
  selector: 'app-celeb-item',
  templateUrl: './celeb-item.component.html',
  styleUrls: ['./celeb-item.component.css']
})
export class CelebItemComponent implements OnInit {
  @Input() celeb: Celeb;
  @Input() currencyValue: string;
  formattedNetWorth = '';

  constructor() { }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges) {
    const { currentValue, previousValue } = changes.currencyValue;

    if (currentValue !== previousValue) {
      this.formatNetWorth();
    }
  }

  formatNetWorth() {
    let code = 'USD';
    if (this.currencyValue === '0.78') {
      code = 'AUD';
    } else if (this.currencyValue === '0.92') {
      code = 'EUR';
    }

    const convertedNetWorth = this.celeb.netWorth * parseFloat(this.currencyValue);
    this.formattedNetWorth = currencyFormatter.format(convertedNetWorth, {
      code: code,
      precision: 0,
      format: `%s${code} %v`
    });
  }

}
