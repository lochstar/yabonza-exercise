import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CelebItemComponent } from './celeb-item.component';

describe('CelebItemComponent', () => {
  let component: CelebItemComponent;
  let fixture: ComponentFixture<CelebItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CelebItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CelebItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
