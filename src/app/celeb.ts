export class Celeb {
  rank: number;
  name: string;
  netWorth: number;
  age: string;
  country: string;
}
