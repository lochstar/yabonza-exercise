import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Celeb } from '../celeb';
import { CelebService } from '../celeb.service';

const richList = require('../../assets/richList.json');

@Component({
  selector: 'app-celebs',
  templateUrl: './celebs.component.html',
  styleUrls: ['./celebs.component.css']
})
export class CelebsComponent implements OnInit {
  celebs: Celeb[];

  birthplaces = richList.celebrityList.map(item => item.country)
    .filter((value, index, self) => self.indexOf(value) === index);

  currencies = [
    { label: 'US Dollar', value: richList.usDollarValue },
    { label: 'Australian Dollar', value: richList.australianDollarValue },
    { label: 'Euro', value: richList.euroValue }
  ];

  orders = [
    { label: 'Rank', value: 'rank' },
    { label: 'Name', value: 'name' },
    { label: 'Age', value: 'age' }
  ];

  birthplaceValue = 'Show All';
  currencyValue = richList.usDollarValue
  searchValue = '';
  orderByValue = 'rank';

  constructor(private celebService: CelebService) { }

  ngOnInit() {
    this.getCelebs();
  }

  getCelebs(): void {
    this.celebService.getCelebs(this.birthplaceValue, this.searchValue, this.orderByValue)
      .subscribe(celebs => this.celebs = celebs);
  }

  onBirthplaceChange(value: string) {
    this.birthplaceValue = value;
    this.getCelebs();
  }

  onCurrencyChange(value: string) {
    this.currencyValue = value;
    this.getCelebs();
  }

  onSearchKeyUp(value: string) {
    this.searchValue = value;
    this.getCelebs();
  }

  onOrderByChange(value: string) {
    this.orderByValue = value;
    this.getCelebs();
  }
}
