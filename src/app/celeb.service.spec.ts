import { TestBed, inject } from '@angular/core/testing';

import { CelebService } from './celeb.service';

describe('CelebService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CelebService]
    });
  });

  it('should be created', inject([CelebService], (service: CelebService) => {
    expect(service).toBeTruthy();
  }));
});
