import { Component } from '@angular/core';

const richList = require('../assets/richList.json');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  pageTitleH1 = richList.pageTitleH1;
  pageTitleH2 = richList.pageTitleH2;
  description = richList.description;
  referenceLink = richList.referenceLink;
}
