import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CelebsComponent } from './celebs/celebs.component';
import { CelebItemComponent } from './celeb-item/celeb-item.component';
import { CelebService } from './celeb.service';

@NgModule({
  declarations: [
    AppComponent,
    CelebsComponent,
    CelebItemComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    CelebService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
