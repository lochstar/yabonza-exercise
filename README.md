# Yabonza Exercise

Demo is available at: [https://www.smashedcrabdev.com/yabonza/](https://www.smashedcrabdev.com/yabonza/)

## Development

Runs a development server accessible at: `http://localhost:4200/`

```bash
npm run start
```

## Build

Outputs a production ready build to `dist/`, open `index.html` in your browser to view.

```bash
npm run build
```

## Notes

- "Country" mispelled in mock-up as "Counrty".
- No tests.
- Not dockerised.

## Timings

- 1hr: set up project, list without filters.
- 1hr: search filter with empty message.
- 30m: select filters UI.
- 45m: currency format & display, birthplace filter, order by and media queries.
- 30m: production build & deployment, README and source control and final touches.

Total: ~3.75hrs
